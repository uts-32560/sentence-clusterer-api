import ujson
import uvicorn
from starlette.applications import Starlette
from starlette.responses import UJSONResponse


from concurrent.futures import ThreadPoolExecutor

import time
import math

import numpy as np

import tensorflow as tf
import tensorflow_hub as hub

from sklearn.cluster import KMeans
from sklearn.metrics.pairwise import cosine_similarity

#import pprint
#pp = pprint.PrettyPrinter(indent=4)

pool = ThreadPoolExecutor(4)

data = {
    'embeddingLoader': None,
    'times': []
}


def loadEmbeddingBuilder():

    print ('loading embedding stuff')

    g = tf.Graph()
    with g.as_default():
        text_input = tf.placeholder(dtype=tf.string, shape=[None])
        embed = hub.Module("https://tfhub.dev/google/universal-sentence-encoder/2")
        embedded_text = embed(text_input)
        init_op = tf.group([tf.global_variables_initializer(), tf.tables_initializer()])

    g.finalize()

    sess = tf.InteractiveSession(graph=g)

    sess.run(init_op)

    print ('embedding stuff ready')

    return lambda sentence: sess.run(embedded_text, feed_dict={text_input: [sentence]})[0]

def loadEmbeddingBuilderAsync(data):

    data['embeddingLoader'] = pool.submit(loadEmbeddingBuilder)



def isReady():

    if not data['embeddingLoader'].done():
        return False


    return True


loadEmbeddingBuilderAsync(data)


app = Starlette()
app.debug = True


def recordProcessingTime(t):

    data['times'].insert(0, t)
    if len(data['times']) > 20:
        data['times'] = data['times'][:20]


def getAvgProcessingTime():

    if len(data['times']) == 0:
        return -1

    return np.mean(data['times'])

def getEmbedding(sentence):

    return data['embeddingLoader'].result()(sentence)

def getSimilarity(embeddingA, embeddingB):

    similarity = cosine_similarity([embeddingA], [embeddingB])[0][0]

    return float(similarity)


@app.route('/objectsimilarities', methods=["GET","POST"])
async def calculateObjectSimilarities(request):

    if not isReady():
        raise AssertionError("Not ready yet!")

    startTime = time.time()

    fields = request.query_params['fields'].split(",")
    objects = await request.json()

    #pp.pprint(objects)

    embeddings = []

    for object in objects:

        embeddedObject = {}

        for field in fields:
            value = object.get(field, "")
            embeddedObject[field] = getEmbedding(value)

        embeddings.append(embeddedObject)

    #pp.pprint(embeddings)

    pairs = []

    for a in range(0,len(objects)):
        for b in range(a+1, len(objects)):

            pair = {
                'idA': objects[a]['id'],
                'idB': objects[b]['id'],
                'similarities': {}
            }

            for field in fields:
                pair['similarities'][field] = getSimilarity(embeddings[a][field], embeddings[b][field])

            pairs.append(pair)

    #pp.pprint(pairs)

    return UJSONResponse(pairs)






@app.route('/clusters', methods=["GET","POST"])
async def clusterSentences(request):

    if not isReady():
        raise AssertionError("Not ready yet!")

    startTime = time.time()


    count = int(request.query_params.get('count', 5))
    req = await request.json()

    if count > len(req['sentences']):
        count = math.floor(len(req['sentences']) / 3)

    result = {
        'clusters': []
    }

    if count > 0:

        embeddings = []
        for sentence in req['sentences']:
            embeddings.append(getEmbedding(sentence))

        kmeans = KMeans(n_clusters=count)
        kmeans.fit(embeddings)

        sentencesByCluster = {}
        for sentence, cluster in zip(req['sentences'], kmeans.labels_):

            if cluster in sentencesByCluster:
                sentencesByCluster[cluster].append(sentence)
            else:
                sentencesByCluster[cluster] = [sentence]

        for cluster, sentencesInCluster in sentencesByCluster.items():
            result['clusters'].append(sentencesInCluster)

    endTime = time.time()

    recordProcessingTime(endTime - startTime)

    return UJSONResponse(result)


@app.route('/status', methods=["GET"])
async def getStatus(request):

    return UJSONResponse({
        'ready': isReady(),
        'averageTime': getAvgProcessingTime()
    })

if __name__ == '__main__':
    uvicorn.run(app, host='0.0.0.0', port=5000)
